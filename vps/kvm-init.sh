#!/bin/bash

#关闭服务
disService(){
systemctl disable  firewalld
systemctl stop  firewalld
for i in abrt-ccpp.service abrt-oops.service abrt-vmcore.service abrt-xorg.service abrtd.service atd.service dbus-org.fedoraproject.firewalld.service dbus-org.freedesktop.NetworkManager.service dbus-org.freedesktop.nm-dispatcher.service firewalld.service kdump.service libstoragemgmt.service lvm2-monitor.service mcelog.service mdmonitor.service microcode.service NetworkManager-dispatcher.service NetworkManager.service postfix.service smartd.service lvm2-lvmetad.socket lvm2-lvmetad.socket lvm2-lvmpolld.socket nfs-client.target remote-fs.target; do systemctl stop $i && systemctl disable $i ; done
}

#安装软件
installApp(){
yum install sysstat ntpdate  lrzsz -y
}

#初始化系统
initOS(){
mkdir -p /data/{shell,app}
echo '* - nofile 65535' >> /etc/security/limits.conf 
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

}

#删除没必要数据库
delUser(){
#删除不必要的用户
userdel adm
userdel lp
userdel sync
userdel shutdown
userdel halt
userdel news
userdel uucp
userdel operator
userdel games
userdel gopher
userdel ftp
groupdel adm   #删除不必要的群组
groupdel lp
groupdel news
groupdel uucp
groupdel games
groupdel dip
groupdel pppusers
}
 
 
 
disService
installApp
initOS
delUser