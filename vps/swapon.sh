#!/bin/bash

#sudo vi /etc/fstab
#/swapfile swap swap defaults 0 0
#sudo swapon --show
#sudo dd if=/dev/zero of=/swapfile bs=1024 count=1048576

sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
