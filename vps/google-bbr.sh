#!/bin/bash


installKernel(){
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum --enablerepo=elrepo-kernel install kernel-ml -y

}

openBBR(){
echo 'net.core.default_qdisc=fq' | tee -a /etc/sysctl.conf
echo 'net.ipv4.tcp_congestion_control=bbr' |  tee -a /etc/sysctl.conf
sysctl -p
}


showKernel(){
rpm -qa | grep kernel
egrep ^menuentry /etc/grub2.cfg | cut -f 2 -d \'
}



rebootOS(){
#设置下标1内核默认启动
grub2-set-default 1
#reboot
}

installKernel
openBBR
showKernel
rebootOS